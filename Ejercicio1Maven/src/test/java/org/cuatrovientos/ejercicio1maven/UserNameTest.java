/**
 * 
 */
package org.cuatrovientos.ejercicio1maven;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class UserNameTest {

	private UserName user; 
	private UserName userEmpty; 
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		user=new UserName("david","madurga");
		userEmpty=new UserName();
	}

	/**
	 * Test method for {@link org.cuatrovientos.ejercicio1maven.UserName#generate()}.
	 */
	@Test
	public void testGenerateEmpty() {
		
		assertEquals("Test generate empty","-empty-",userEmpty.generate());
	}

	/**
	 * Test method for {@link org.cuatrovientos.ejercicio1maven.UserName#generate()}.
	 */
	@Test
	public void testGenerate() {

		assertEquals("Test generate no empty","david_madurga",user.generate());
	}

	

}
