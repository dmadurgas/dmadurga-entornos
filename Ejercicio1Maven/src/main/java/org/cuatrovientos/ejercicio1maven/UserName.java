/**
 * 
 */
package org.cuatrovientos.ejercicio1maven;

/**
 * create username
 * @author david_madurga
 *
 */
public class UserName {

	private String name;
	private String surname;
	
	/**
	 * Default constructor
	 */
	public UserName(){
		name="";
		surname="";
	}
	
	/**
	 * Constructor with args
	 * @param name
	 * @param surname
	 */
	public UserName(String name,String surname){
		this.name=name;
		this.surname=surname;
	}
	
	/**
	 * create a username
	 */
	public String generate() {
		if (name.equals("") && surname.equals("")){
			return "-empty-";
		}
		return name.toLowerCase() + "_" + surname.toLowerCase();
	}
}
