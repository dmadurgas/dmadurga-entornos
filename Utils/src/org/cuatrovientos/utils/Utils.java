/**
 * 
 */
package org.cuatrovientos.utils;

/**
 * IsNumeric of visual
 * @author david_madurga
 *
 */
public class Utils {

	/**
	 * @param args
	 */
	public static boolean isNumeric(String args) {
		
		try{
			Integer.parseInt(args);
			return true;
		} catch(NumberFormatException nfe) {
			return false;
		}
		
	}

}
