/**
 * 
 */
package org.cuatrovientos.exercise2if;

import java.util.Scanner;

/**
 * exercise 2 about structure control
 * @author david_madurga
 * @version 1.0
 */
public class Exercise2If {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String line="";
		
		
		Scanner reader= new Scanner(System.in);
		
		System.out.print("Enter a integer num ");
		line = reader.nextLine();
		
		try{ //test the sentence
			
			if ((Integer.parseInt(line) % 2)==0){
				System.out.println("the number is pair");
			} else {
				System.out.println("the number is impair");
			}
		} catch (NumberFormatException e){ //exception
			System.out.println("Error unknow");
		}
		
		reader.close();
		
	}

}
