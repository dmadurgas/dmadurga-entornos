package org.cuatrovientos.hellokitty;

public class Sweet extends Food{
	private int calories;
	
	/**
	 * Constructor
	 * @param name
	 * @param height
	 * @param calories
	 */
	public Sweet(String name, float height, int calories) {
		super(name, height);
		this.calories=calories;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + "calories: " + calories + "]";
	}
	
	
	
	
}
