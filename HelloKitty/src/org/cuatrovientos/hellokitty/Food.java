package org.cuatrovientos.hellokitty;

public class Food {
	
	protected String name;
	protected float height=0f;
	/**
	 * @param name
	 * @param height
	 */
	public Food(String name, float height) {
		this.name = name;
		this.height = height;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(float height) {
		this.height = height;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Food [name=" + name + ", height=" + height + "]";
	}
	
	
}
