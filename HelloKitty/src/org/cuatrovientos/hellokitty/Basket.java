package org.cuatrovientos.hellokitty;

import java.util.Vector;

public class Basket {

	protected Vector<Food> buy;

	/**
	 * Constructor
	 * @param buy
	 */
	public Basket() {
		this.buy = new Vector<Food>();
	} 
	
	public void getFood (Food c){
		buy.add(c);
	}
	
	public float heightAll(){
		float p=0f;
		for (Food f : buy){
			p+=f.height;
		}
		return p;
	}
	
	public String toString () {
		
		
		return buy.toString();
		
	}
	
	
}
