package org.cuatrovientos.hellokitty;

public class Fruit extends Food {

	private String vitamins;
	/**
	 * Constructor
	 * @param name
	 * @param height
	 * @param vitamins
	 */
	public Fruit(String name, float height, String vitamins) {
		super(name, height);
		this.vitamins=vitamins;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Fruit [vitamins=" + vitamins + ", name=" + name + ", height="
				+ height + "]";
	}
	
	

}
