/**
 * 
 */
package org.cuatrovientos.hellokitty;

import java.util.Scanner;
import java.util.Vector;

/**
 * @author david_madurga
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vector<Food> myFood = new Vector<Food>();
		Basket myBasket = new Basket();
		String line="";
		String name="";
		float height=0f;
		String vitamines="";
		int calories=0;
		Scanner reader = new Scanner(System.in);
		
		do {
			
			System.out.println("Chosee option:");
			System.out.println("1-Get Fruit ");
			System.out.println("2-Get Sweet");
			System.out.println("3-Get Food");
			System.out.println("4-Exit");
			line=reader.nextLine();
			
			switch(line){
			case "1":
				
				
				System.out.println("Enter name of fruit:");
				name=reader.nextLine();
				
				System.out.println("Enter height of fruit:");
				height=Float.parseFloat((reader.nextLine()));
				
				System.out.println("Enter vitamins of fruit:");
				vitamines=reader.nextLine();
				
				Fruit myFruit = new Fruit(name,height,vitamines);
				myBasket.getFood(myFruit);
				break;
				
			case "2":
				
				System.out.println("Enter name of Sweet:");
				name=reader.nextLine();
				
				System.out.println("Enter height of Sweet:");
				height=Float.parseFloat((reader.nextLine()));
				
				System.out.println("Enter calories of Sweet:");
				calories=Integer.parseInt(reader.nextLine());
				
				Sweet mySweet = new Sweet(name,height,calories);
				myBasket.getFood(mySweet);
				break;
				
			case "3":
				
				System.out.println("Enter name of Food:");
				name=reader.nextLine();
				
				System.out.println("Enter height of Food:");
				height=Float.parseFloat((reader.nextLine()));
				
				
				Food myNewFood = new Food(name,height);
				myBasket.getFood(myNewFood);
				break;
				
			}
			
			
		} while ( Integer.parseInt(line)!=4);
		
		System.out.println(myBasket.toString());
		System.out.println(myBasket.heightAll());
		
		reader.close();

	}

}
