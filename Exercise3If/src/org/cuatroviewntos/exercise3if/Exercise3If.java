/**
 * 
 */
package org.cuatroviewntos.exercise3if;

import java.util.Scanner;

/**
 * exercise 3 structures control
 * @author david_madurga
 * @version 1
 */
public class Exercise3If {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String line = "";
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter number positive and integer: ");
		line = reader.nextLine();
		
		try {
			if (Integer.parseInt(line) > 0 && (Integer.parseInt(line)%2)==0) {
				System.out.println("Number is positive and even");
			} else {
				System.out.println("Number isn�t positive or even");
			}
		} catch (NumberFormatException e) {
			System.out.println("Error unknown");
		}
		
		reader.close();
	}

}
