/**
 * 
 */
package org.cuatrovientos.cadenas;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class CadenasTest {

	/**
	 * @throws java.lang.Exception
	 */
	public static Cadenas myCadena;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 myCadena = new Cadenas();
	}

	/**
	 * Test method for {@link org.cuatrovientos.cadenas.Cadenas#lenght(java.lang.String)}.
	 */
	@Test
	public void testLenght() {
		int target=myCadena.lenght("hola");
		int expected=4;
		assertEquals("Test length method",expected,target);
				
	}

	/**
	 * Test method for {@link org.cuatrovientos.cadenas.Cadenas#vocales(java.lang.String)}.
	 */
	@Test
	public void testVocales() {
		int target=myCadena.vocales("hola");
		int expected=2;
		assertEquals("Test vocales method",expected,target);
				
	}

	/**
	 * Test method for {@link org.cuatrovientos.cadenas.Cadenas#reverse(java.lang.String)}.
	 */
	@Test
	public void testReverse() {
		String target=myCadena.reverse("hola");
		String expected=("aloh");
		assertEquals("Test reverse method",expected,target);
				
	}

	/**
	 * Test method for {@link org.cuatrovientos.cadenas.Cadenas#countLetter(java.lang.String, char)}.
	 */
	@Test
	public void testCountLetter() {
		int target=myCadena.countLetter("hola",'o');
		int expected=1;
		assertEquals("Test count letter method",expected,target);
				
	}

}
