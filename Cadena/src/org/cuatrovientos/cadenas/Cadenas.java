/**
 * 
 */
package org.cuatrovientos.cadenas;

/**
 * @author david_madurga
 *
 */
public class Cadenas {
	
	
	public int lenght (String cadena){
		return cadena.length();
	}
	
	public int vocales (String cadena){
		int counter =0;
		char[] vocals={'a','e','i','o','u'};
		for (int i =0; i < cadena.length();i++){
			for (char v: vocals ){
				if (v==cadena.toLowerCase().charAt(i)){
					counter++;
				}
			}
			
		}
		return counter;
	}
	
	public String reverse (String cadena){
		String line="";
		for (int i =cadena.length()-1; i>=0;i--){
			line+=(cadena.charAt(i));
		}
		return line;
	}
	
	public int countLetter(String cadena, char letter){
		int counter=0;
		for (int i =0;i< cadena.length();i++){
			if (cadena.charAt(i)==letter){
				counter++;
			}
		}
		return counter;
	}
}
