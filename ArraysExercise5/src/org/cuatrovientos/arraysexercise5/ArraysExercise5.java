/**
 * 
 */
package org.cuatrovientos.arraysexercise5;

import java.util.Scanner;


/**
 * exercise arrays 5
 * @author david_madurga
 *
 */
public class ArraysExercise5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int [] num = {0, 0, 0, 0, 0, 0, 0, 0, 0};
		Scanner reader = new Scanner (System.in);
		float average = 0f;
		String line ="";
		
		for (int i=0; i< num.length;i++) {
			
			System.out.println("Enter a number");
			line = reader.nextLine();
			num[i]=Integer.parseInt(line);		
			
		}
		
		for (int i=0; i< num.length;i++) {
			System.out.println(num[i]);
			average=num[i];
		
		}
		System.out.println("Average: " + average/num.length);
		reader.close();
		
		
		
	}

}
