/**
 * 
 */
package org.cuatrovientos.loopsexercise6;

import java.util.Scanner;
/**
 * @author david_madurga
 *
 */
public class LoopsExercise6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String line = "";
		int num = 0;
		int factorial = 1;
		
		System.out.print("Enter a integer number ");
		line=reader.nextLine();
		
		num = Integer.parseInt(line);
		
		for (int i = 2; i<=num; i++) {
			
			factorial*= i;

		}
		
		System.out.println("The factorial of " + num + " is : " + factorial);
		
		reader.close();
						
	}

}
