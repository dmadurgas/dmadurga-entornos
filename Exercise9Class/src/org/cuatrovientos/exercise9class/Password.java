package org.cuatrovientos.exercise9class;

import java.util.Random;

public class Password {
	
	private String characters="qazwsxedcrfvtgbyhnujmiklop�QAZWSXEDCRFVTGBYHNUJMIKOLP�1234567890!�$%&/()=";
	private int Longitude=8;
	private String pass=""; 
	
	public Password() {
		
	}
	
	public Password(int longitude){
		this.Longitude=longitude;
	}
	
	public void generate(){
		Random rnd = new Random();
		for (int i = 0; i < this.Longitude; i++ ) {
			this.pass+= this.characters.charAt(rnd.nextInt(characters.length()));
		}
		System.out.println("Password: " + this.pass);
		this.pass="";
	}
	
	public void generate (int replies){
		Random rnd = new Random();
		for(int i = 0; i< replies; i++){
			
			for (int j = 0; j < this.Longitude; j++ ) {
				this.pass+= this.characters.charAt(rnd.nextInt(characters.length()));
			}
			System.out.println("Password: " + this.pass);
			this.pass="";
	
		}
	}
	
}
