package org.cuatrovientos.englishquiz;

public class Question {
	
	private String questName;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private String solution;
	
	/**
	 * Constructor
	 * @param questName
	 * @param option1
	 * @param option2
	 * @param option3
	 * @param option4
	 * @param solution
	 */
	public Question(String questName, String option1, String option2,
			String option3, String option4, String solution) {
		this.questName = questName;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.solution = solution;
	}
	


	/**
	 * @return the option2
	 */
	String getOption2() {
		return option2;
	}

	/**
	 * @param option2 the option2 to set
	 */
	void setOption2(String option2) {
		this.option2 = option2;
	}

	/**
	 * @return the questname
	 */
	String getQuestname() {
		return questName;
	}

	/**
	 * @param questname the questname to set
	 */
	void setQuestname(String questname) {
		this.questName = questname;
	}

	/**
	 * @return the option1
	 */
	String getOption1() {
		return option1;
	}

	/**
	 * @param option1 the option1 to set
	 */
	void setOption1(String option1) {
		this.option1 = option1;
	}

	/**
	 * @return the option3
	 */
	String getOption3() {
		return option3;
	}

	/**
	 * @param option3 the option3 to set
	 */
	void setOption3(String option3) {
		this.option3 = option3;
	}

	/**
	 * @return the option4
	 */
	String getOption4() {
		return option4;
	}

	/**
	 * @param option4 the option4 to set
	 */
	void setOption4(String option4) {
		this.option4 = option4;
	}

	/**
	 * @return the solution
	 */
	String getSolution() {
		return solution;
	}

	/**
	 * @param solution the solution to set
	 */
	void setSolution(String solution) {
		this.solution = solution;
	}

	
	
	

}
