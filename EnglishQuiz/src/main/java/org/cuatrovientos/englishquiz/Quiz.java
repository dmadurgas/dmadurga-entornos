package org.cuatrovientos.englishquiz;

import java.util.Scanner;
import java.util.Vector;

public class Quiz {
	private static final int TOTAL_QUESTIONS = 10;
	private Vector<Question> questions;
	private int correcto;
	private String name;
	
	/**
	 * Constructor
	 * @param name
	 */
	public Quiz (String name) {
		setQuestions(new Vector<Question>());
		setCorrect(0);
		setName(name);
		init();
	}
	/**
	 * Constructor alternative
	 * @param name
	 */
	public Quiz (String name, boolean start) {
		setQuestions(new Vector<Question>());
		setCorrect(0);
		setName(name);
	}
	
	/**
	 * inits vector with questions
	 */
	public void init () {
		addQuestion(new Question("We study __ the university","in","on","at","into","at"));
		addQuestion(new Question("We study __ the school","in","on","at","into","at"));
		addQuestion(new Question("Tyrion is __ prison","in","on","at","into","in"));
		addQuestion(new Question("Tyson was  __ jail","in","on","at","into","in"));
		addQuestion(new Question("I was  __ the beach","in","on","at","into","on"));
		addQuestion(new Question("Turn __ the left","in","on","at","into","on"));
		addQuestion(new Question("I was waiting __ the corner","in","on","at","into","at"));
		addQuestion(new Question("We met __ the theatre","in","on","at","into","at"));
		addQuestion(new Question("It depends  __ the weather","in","on","at","off","in"));
		addQuestion(new Question("We went  __ train","in","on","onto","by","by"));
	}
	
	/**
	 * insert Question
	 * @param q
	 */
public void addQuestion (Question q) {
		getQuestions().add(q);
	}
	/**
	 * delete a Question
	 * @param questionName
	 */
	public void deleteQuestion (String questionName) {
		for (int i = 0; i < quizLength(); i++){
			if (getQuestions().elementAt(i).getQuestname()==questionName){
		
				getQuestions().remove(i);
			}
		}
		
	}
	/**
	 * shoq size of Quiz
	 * @return size
	 */
public int quizLength () {
		return getQuestions().size();
	}

	/**
	 * Start Quiz
	 */
	public void makeQuiz () {
		Scanner reader = new Scanner(System.in);
		String line = "";
		int numberQuestion = 1;
		
		for (Question q :this.getQuestions()) {
			System.out.println("Question " + numberQuestion + ": " + q.getQuestname());
			System.out.println("Options ");
			System.out.println(q.getOption1());
			System.out.println(q.getOption2());
			System.out.println(q.getOption3());
			System.out.println(q.getOption4());
			System.out.print("Write the correct: ");
			line = reader.nextLine();
			
			checkAnswer(line, q);
			
			numberQuestion++;
		}
		
		// Change this to compare with the size of the Vector
		calification();
	}

	/**
	 * Show result
	 */
	private void calification() {
		if (getCorrect() >= 5) {
			System.out.println("Congratulations, you passed the exam with " + getCorrect() + " correct answers");
		} else {
			System.out.println("You failed: " + getCorrect() + ". Better luck next time");			
		}
	}

	/**
	 * Check Answer
	 * @param line
	 * @param q
	 */
	private void checkAnswer(String line, Question q) {
		if (line.toLowerCase().equals(q.getSolution())) {
			setCorrect(getCorrect() + 1);
			System.out.println("Ok, That was correct");
			System.out.println("Correct answers: " + getCorrect() + " out of " + TOTAL_QUESTIONS);
		} else {
			System.out.println("Wrong answer. The correct was: ");
			System.out.println(q.getSolution());
			System.out.println("Correct answers: " + getCorrect() + " out of " + TOTAL_QUESTIONS);
		}
	}

	/**
	 * @return the questions
	 */
	Vector<Question> getQuestions() {
		return questions;
	}

	/**
	 * @param questions the questions to set
	 */
	void setQuestions(Vector<Question> questions) {
		this.questions = questions;
	}

	/**
	 * @return the cORRec
	 */
	int getCorrect() {
		return correcto;
	}

	/**
	 * @param correct the cORRec to set
	 */
	void setCorrect(int correct) {
		correcto = correct;
	}

	/**
	 * @return the name
	 */
	String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	void setName(String name) {
		this.name = name;
	}
}
