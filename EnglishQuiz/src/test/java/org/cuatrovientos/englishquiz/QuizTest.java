/**
 * 
 */
package org.cuatrovientos.englishquiz;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class QuizTest {

	public Quiz testQuiz;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test Empty Quiz
	 * Test method for {@link org.cuatrovientos.englishquiz.Quiz#Quiz(java.lang.String)}.
	 */
	@Test
	public void testQuizEmtpy() {
		testQuiz = new Quiz("test",true);
		int target = testQuiz.quizLength();
		int expected = 0;
		assertEquals("Test Empty Quiz", target, expected);
	}

	/**
	 * Test one Question in Quiz
	 * Test method for {@link org.cuatrovientos.englishquiz.Quiz#addQuestion(org.cuatrovientos.englishquiz.Question)}.
	 */
	@Test
	public void testAddQuestion() {
		testQuiz = new Quiz("test",true);
		testQuiz.addQuestion(new Question("We study __ the university","in","on","at","into","at"));
		int target = testQuiz.quizLength();
		int expected = 1;
		assertEquals("Test one Question in Quiz", target, expected);
	}

	/**
	 * Test delete Question
	 * Test method for {@link org.cuatrovientos.englishquiz.Quiz#deleteQuestion(java.lang.String)}.
	 */
	@Test
	public void testDeleteQuestion() {
		testQuiz = new Quiz("test", true);
		testQuiz.addQuestion(new Question("We study __ the university","in","on","at","into","at"));
		testQuiz.deleteQuestion("We study __ the university");
		int target = testQuiz.quizLength();
		int expected = 0;
		assertEquals("Test delete Question", target, expected);
		
	}

}
