/**
 * 
 */
package org.cuatrovientos.input;

import java.util.Scanner;

/**
 * class to test console input
 * @author david_madurga
 * @version 1.0
 */
public class Input {

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		String name = "";
		int age = 0;
		Scanner reader = new Scanner(System.in);
		System.out.print("Please, enter the name ");
		name = reader.nextLine();
		System.out.println("Hi " + name);
		System.out.print("How old are you? ");
		age = Integer.parseInt(reader.nextLine()); /* to convert to int */
		System.out.print("you are " + age + " years old");
		reader.close();

	}

}
