package org.cuatrovientos.caesarcipher;


/**
 * implements caeser cipher system
 * @author Caius Contomatus, legio XIII
 *
 */
public class CaesarCipher {
	private int desplazamiento;
	private String alfabetic = "abcdefghijklmnopqrstuvwxyz";
	
	/**
	 * @return the alfabetic
	 */
	String getAlfabetic() {
		return alfabetic;
	}

	/**
	 * @param alfabetic the alfabetic to set
	 */
	void setAlfabetic(String alfabetic) {
		this.alfabetic = alfabetic;
	}

	/**
	 * @return the desplazamiento
	 */
	int getDesplazamiento() {
		return desplazamiento;
	}

	/**
	 * @param desplazamiento the desplazamiento to set
	 */
	void setDesplazamiento(int desplazamiento) {
		this.desplazamiento = desplazamiento;
	}

	public CaesarCipher (int desplazamiento) {
		this.setDesplazamiento(desplazamiento);
	}
	
	/**
	 * encrypts a String using caser cipher
	 * @param word
	 * @return
	 */
	public String encrypt (String word) {
		String encryptWord = "";

		word = word.toLowerCase();
		
		encryptWord = generate(word, encryptWord);
		
		return encryptWord;
	}

	/**
	 * @param word
	 * @param encryptWord
	 * @return
	 */
	private String generate(String word, String encryptWord) {
		int position=0;
		for (int i=0;i<word.length();i++) {
			position = getAlfabetic().indexOf(word.charAt(i));
			position = (position + getDesplazamiento()) % 26; 
			encryptWord += getAlfabetic().charAt(position);
		}
		return encryptWord;
	}
	
	/**
	 * decrypts a String using caser cipher
	 * @param word
	 * @return
	 */
	public String decrypt (String word) {
		String decryptWord = "";
		word = word.toLowerCase();
		
		
		decryptWord = desgenerate(word, decryptWord);
		
		return decryptWord;
	}

	/**
	 * @param word
	 * @param decryptWord
	 * @return
	 */
	private String desgenerate(String word, String decryptWord) {
		int position=0;
		for (int i=0;i<word.length();i++) {
			position = getAlfabetic().indexOf(word.charAt(i));
			position = (26 + (position - getDesplazamiento())) % 26; 
			decryptWord += getAlfabetic().charAt(position);
		}
		return decryptWord;
	}

}
