package org.cuatrovientos.caesarcipher;

/**
 * Main class for testing my cipher method
 * @author Julius Caesar
 *
 */
public class caesarMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CaesarCipher cc = new CaesarCipher(3);
		System.out.println(cc.encrypt("avecaesar"));
		System.out.println(cc.decrypt("dyhfdhvdu"));
		
	}

}
