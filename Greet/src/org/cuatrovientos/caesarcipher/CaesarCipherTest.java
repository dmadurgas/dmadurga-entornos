/**
 * 
 */
package org.cuatrovientos.caesarcipher;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class CaesarCipherTest {

	private static CaesarCipher myCipher;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		myCipher = new CaesarCipher(3);
	}

	/**
	 * Test method for {@link org.cuatrovientos.caesarcipher.CaesarCipher#CaesarCipher(int)}.
	 */
	@Test
	public void testCaesarCipher() {
		int expected=3;
		assertEquals("Test Constructor",expected,myCipher.getDesplazamiento());

	}

	/**
	 * Test method for {@link org.cuatrovientos.caesarcipher.CaesarCipher#encrypt(java.lang.String)}.
	 */
	@Test
	public void testEncrypt() {
		String target=myCipher.encrypt("avecaesar");
		String expected="dyhfdhvdu";
		assertEquals("Test Encrypt",expected,target);
		
	}

	/**
	 * Test method for {@link org.cuatrovientos.caesarcipher.CaesarCipher#decrypt(java.lang.String)}.
	 */
	@Test
	public void testDecrypt() {
		String target=myCipher.decrypt("dyhfdhvdu");
		String expected="avecaesar";
		assertEquals("Test Dencrypt",expected,target);

	}

}
