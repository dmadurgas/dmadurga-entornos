package org.cuatrovientos.password;

import java.util.Random;

public class RandomPasswordParameter {
	public int longitude;
	public String password;
	public String characters;
	public Random random;
	public int longitudeChar;

	public RandomPasswordParameter(int longitude, String password,
			String characters, Random random, int longitudeChar) {
		this.longitude = longitude;
		this.password = password;
		this.characters = characters;
		this.random = random;
		this.longitudeChar = longitudeChar;
	}
}