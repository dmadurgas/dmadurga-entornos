package org.cuatrovientos.password;
import java.util.Random;


/**
 * Password Generator
 * @author Programador de mal gusto
 * @greets a D. Cobol por la inspiración
 */
public class Password {
	/**
	 * Generate password with length
	 * genera contraseñas largas y duras como un pene
	 * @param longitude
	 * @return
	 */
	public String generatePassword (int longitude) {
		String password = "";
		String characters = "abcdefghijklmnopqrstuvwxyz0123456789";
		Random random = new Random();
		int longitudeChar = characters.length();
		if (longitude < 8 || longitude > 20) {
			return "Wrong Longitude";
		}
		
		password = randomPassword(new RandomPasswordParameter(longitude, password, characters, random, longitudeChar));
		
		return password;
	}

	/**
	 * @param parameterObject TODO
	 * @return
	 */
	private String randomPassword(RandomPasswordParameter parameterObject) {
		String password = parameterObject.password;
		for (int i=0;i<parameterObject.longitude;i++) {
			password += parameterObject.characters.charAt(parameterObject.random.nextInt(parameterObject.longitudeChar));
		}
		return password;
	}
}
