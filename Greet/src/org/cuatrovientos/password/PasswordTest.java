package org.cuatrovientos.password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordTest {

	@Test
	public void testGeneratePassword() {
		Password myPassword = new Password();
		
		String target = myPassword.generatePassword(10);
		int expected = 10;
		assertEquals("Check password length",target.length(),expected);
	}
	
	@Test
	public void testWrongPassword() {
		Password myPassword = new Password();
		
		String target = myPassword.generatePassword(4);
		String expected = "Wrong Longitude";
		assertEquals("Check password length",target,expected);
	}

}
