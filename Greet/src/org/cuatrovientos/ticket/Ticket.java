package org.cuatrovientos.ticket;

/**
 * 
 * Clase para hacer facturas y aplicar iva
 * @author Programador Bajeril
 *
 */
public class Ticket {
	private static final double SPECIAL_DISCOUNT = 0.10d;
	private static final double IVA_NORMAL = 0.21d;
	private static final double IVA_REDUCED = 0.04d;
	private double total;
	private double subtotal;
	
	public Ticket (double prize) {
		setSubtotal(prize);
	}
	
	/**
	 * @return the total
	 */
	double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the subtotal
	 */
	public double getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 *  le aplica al total el iva reducido
	 *  y se guarda en el total
	 */
	public void ivaReducido () {
		setTotal(getSubtotal() + (getSubtotal() * IVA_REDUCED));
	}

	/**
	 *  le aplica al total el iva normal
	 *  y se guarda en el total
	 */
	public void ivaNormal () {
		setTotal(getSubtotal() + (getSubtotal() * IVA_NORMAL));		
	}
	
	/**
	 * le aplica un descuento especial fijo
	 * que se aplica a clientes VIP
	 */
	public void descountStatic () {
		setTotal(getSubtotal() - (getSubtotal() * SPECIAL_DISCOUNT));
	}

}
