/**
 * 
 */
package org.cuatrovientos.ticket;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class TicketTest {

	private static Ticket myTicket;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		myTicket = new Ticket(2d);
	}

	/**
	 * Test method for {@link org.cuatrovientos.ticket.Ticket#ivaReducido()}.
	 */
	@Test
	public void testIvaReducido() {
		double expected=2d+2d*0.04d;
		myTicket.ivaReducido();
		double target=myTicket.getTotal();
		assertEquals("Testing Iva reduced",expected,target,0.0d);
	}

	/**
	 * Test method for {@link org.cuatrovientos.ticket.Ticket#ivaNormal()}.
	 */
	@Test
	public void testIvaNormal() {
		double expected=2d+2d*0.21d;
		myTicket.ivaNormal();
		double target=myTicket.getTotal();
		assertEquals("Testing Iva normal",expected,target,0.0d);
	}

	/**
	 * Test method for {@link org.cuatrovientos.ticket.Ticket#descountStatic()}.
	 */
	@Test
	public void testDescountStatic() {
		double expected=2d-2d*0.1d;
		myTicket.descountStatic();
		double target=myTicket.getTotal();
		assertEquals("Testing Iva reduced",expected,target,0.0d);
	}

}
