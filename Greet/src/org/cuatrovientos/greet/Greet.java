package org.cuatrovientos.greet;
/**
 * 
 */

/**
 * @author Bad Taste
 * Greets in different languages
 */
public class Greet {

	/**
	 * Greets 
	 * @param language chosen language
	 * @param repetitions how many times repeat greet
	 * @return a String with the greeting
	 */
	public String greetRepeatedly(int language, int repetitions) {
		String greeting = "";
		String hello = "";
		
		hello = selectGreet(language, hello);
		return greetRepetitions(repetitions, greeting, hello);
	}

	/**
	 * @param language
	 * @param hello
	 * @return
	 */
	private String selectGreet(int language, String hello) {
		switch (language) {
			case 0 : hello = "hola";
					 break;
			case 1 : hello = "hello";
					 break;
			case 2 : hello = "kaixo";
			 		 break;
			 default:
				 	 break;	
		 }
		return hello;
	}

	/**
	 * Repeat greet
	 * @param repetitions
	 * @param greeting
	 * @param hello
	 * @return
	 */
	private String greetRepetitions(int repetitions, String greeting,
			String hello) {
		for (int i=0;i<repetitions;i++) {
			greeting += hello;
		}
		
		return greeting;
	}
}
