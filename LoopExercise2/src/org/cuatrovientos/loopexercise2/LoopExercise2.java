/**
 * 
 */
package org.cuatrovientos.loopexercise2;

import java.util.Scanner;

/**
 * exercise loop 2
 * @author david_madurga
 *
 */
public class LoopExercise2 {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
			String line = "";
			int number = 0;
			Scanner reader = new Scanner(System.in);
			//do {
			System.out.print("Enter number integer; ");
			line=reader.nextLine();
			//}while isNumeric(line)!=false;
			number = Integer.parseInt(line);
			
			if (number > 0 && (number%2 == 0)){
				for (int i =0; i<number;i++){
					System.out.print("*");
				}
				
			}else{
				System.out.println("Error you don�t requisites");
			
			}
			
			reader.close();
			return;
	}

}
