/**
 * 
 */
package org.cuatrovientos.methods;

/**
 * sample methods
 * @author david_madurga
 *
 */
public class Methods {

	
	
	/**
	 * say thing in loop
	 * @param loop
	 * @param something
	 */
	public static void sayLoop(Integer loop,String something) {
		for(int i=0;i<loop;i++) {
			saySomething(something);
		}
	}
	/**
	 *	Shows hello on console
	 * 
	 */
	public static void sayHello() {
		System.out.println("Say hello my young padawan");
		return; 
	}
	
	/**
	 * Shows thing on console
	 * @param something
	 * 
	 */
	public static void saySomething(String something){
		System.out.println(something);
	}
	/**
	 * Shows number on console
	 * @param something
	 */
	public static void saySomething(int something){
		System.out.println(something);
	}
	/**
	 * sample sum
	 * @param x
	 * @param y
	 * @return
	 */
	public static int add(int x, int y){
		return x+y;
	}
	
	/**
	 * begin array with 0
	 * @param value
	 */
	public static void init (int[] value) {
		
		for (int i = 0; i< value.length;i++) {
			value[i]=0;
		}
	}
	
	/**
	 * show values of array
	 * @param value
	 */
public static  void show (int[] value) {
		
		for (int i = 0; i< value.length;i++) {
			System.out.println(value[i]);
		}
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Methods.sayHello();
		Methods.saySomething("What the power is in you");
		Methods.sayLoop(5, "FAP");
		Methods.saySomething(Methods.add(5,6));
		int [] numbers = new int[10];
		Methods.init(numbers);
		Methods.show(numbers);
	}

}
