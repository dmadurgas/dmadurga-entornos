/**
 * 
 */
package org.cuatrovientos.exercise6operators;

import java.util.Scanner;

/**
 * conversion dolars to euros
 * @author david_madurga
 *
 */
public class Exercise6Operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String numberString = "";
		float numberResult = 0;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter the dolars: ");
		numberString = reader.nextLine();
		numberResult = (float)Integer.parseInt(numberString);
		numberResult = numberResult / 14.9F;
		
		System.out.print("The dolars: " + numberResult);	
		reader.close();

	}

}
