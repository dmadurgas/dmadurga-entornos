/**
 * 
 */
package org.cuatrovientos.loopsexercise8;

import java.util.Scanner;

/**
 * cousin number
 * @author david_madurga
 *
 */
public class LoopsExercise8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner reader = new Scanner (System.in);
		String line = "";
		int number = 0;
		int cont =0;
		
		System.out.print("Enter a integer number: ");
		line = reader.nextLine();
		
		number = Integer.parseInt(line);
		
		for (int i = 1; i <=number;i++) {
			
			if (number%i==0) {
				cont+=1;
			}
			
				
		}
		if (cont > 2){
		System.out.println("The number isn�t prime");
		} else {
			System.out.println("The number is prime");
		}
		reader.close();
		
		
	}

}
