/**
 * 
 */
package org.cuatrovientos.exercise4if;

import java.util.Scanner;

/**
 * exercise 4 structures control
 * @author david_madurga
 * @version1
 * 
 */
public class Exercise4If {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String lineUser1 = "";
		String lineUser2 = "";
		int number1 = 0;
		int number2 = 0;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter the 1� number : ");
		lineUser1 = reader.nextLine();
		
		System.out.print("Enter the second number: ");
		lineUser2 = reader.nextLine();
		
		try{
			number1 = Integer.parseInt(lineUser1);
			number2 = Integer.parseInt(lineUser2);
			
			if (number1 > number2) {
				System.out.println("Number 1 is bigger");
			}else {
				
				if (number1 < number2) {
					System.out.println("Number 2 is bigger");
				} else {
					System.out.println("Number 2 is equal than Number 1");
				}
			}
			
		} catch (NumberFormatException nfe) {
			System.out.println("Error in Number Format idiot");
		
		}
		
		
		
		reader.close();
	}

}
