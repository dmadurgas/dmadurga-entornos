package org.cuatrovientos.exercise6class;

public class Square {

	private char image=' ';
	
	public Square() {
		this.image='#';
	}
	
	public Square(char image){
		this.image=image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(char image) {
		this.image = image;
	}
	
	private String generate(int tall){
		String line="";
		for (int i = 0; i < tall; i++){
			for(int j = 0; j < tall; j++){
				line+=image;
			}
			line+="\n";
		}
		return line;
	}
	
	public void make(int tall){
		System.out.print(generate(tall));
	}
	
}
