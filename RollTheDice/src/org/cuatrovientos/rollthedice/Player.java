/**
 * 
 */
package org.cuatrovientos.rollthedice;

import java.util.Random;

/**
 * @author david_madurga
 *
 */
public class Player {
	private int strength;
	private int speed;
	private int inteligence;
	private int lifepoints;
	private int resistance;
	private String name;
	
	/**
	 * Constructor
	 */
	public Player() {
		Random random = new Random();
		for (int i = 0; i < 30 ; i++) {
			switch(random.nextInt(4)){
			
			}
		}
	}
	/**
	 * @param strength
	 * @param speed
	 * @param inteligence
	 * @param lifepoints
	 * @param resistance
	 * @param name
	 */
	public Player(int strength, int speed, int inteligence, int lifepoints,
			int resistance, String name) {
		this.strength = strength;
		this.speed = speed;
		this.inteligence = inteligence;
		this.lifepoints = lifepoints;
		this.resistance = resistance;
		this.name = name;
	}
	/**
	 * calculate attack
	 * @param value
	 * @return damage points
	 */
	public int attack() {
		Dice dice=new Dice(10);
		return strength*dice.roll();
	}
	
	
	
	/**
	 * @return the strength
	 */
	public int getStrength() {
		return strength;
	}
	/**
	 * @param strength the strength to set
	 */
	public void setStrength(int strength) {
		this.strength = strength;
	}
	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}
	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	/**
	 * @return the inteligence
	 */
	public int getInteligence() {
		return inteligence;
	}
	/**
	 * @param inteligence the inteligence to set
	 */
	public void setInteligence(int inteligence) {
		this.inteligence = inteligence;
	}
	/**
	 * @return the lifepoints
	 */
	public int getLifepoints() {
		return lifepoints;
	}
	/**
	 * @param lifepoints the lifepoints to set
	 */
	public void setLifepoints(int lifepoints) {
		this.lifepoints = lifepoints;
	}
	/**
	 * @return the resistance
	 */
	public int getResistance() {
		return resistance;
	}
	/**
	 * @param resistance the resistance to set
	 */
	public void setResistance(int resistance) {
		this.resistance = resistance;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
