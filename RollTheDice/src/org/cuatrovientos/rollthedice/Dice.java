/**
 * 
 */
package org.cuatrovientos.rollthedice;

/**
 * Represents a dice
 * @author david_madurga
 *
 */
public class Dice {

	//attributes
	private int sides;
	
	//constructor
	public Dice () {
		this.sides=6;
	}
	
	
	/**
	 * create dice with sides
	 * @param sides
	 */
	public Dice (int sides) {
		this.sides=sides;
	}
	//methods
	/**
	 * rolls the dice
	 * @return value
	 */
	public int roll () {
		
		/* Other method for create random numbers but it begin in one*/
		return (int) (Math.random()*this.sides+1);
	}

	/**
	 * @return the sides
	 */
	public int getSides() {
		return sides;
	}

	/**
	 * @param sides the sides to set
	 */
	public void setSides(int sides) {
		this.sides = sides;
	}
}
