/**
 * 
 */
package org.cuatrovientos.rollthedice;

/**
 * A player
 * @author david_madurga
 *
 */
public class Warrior extends Player{
	
	public  Warrior () {
		super(10, 5, 3, 100,
				8, "Kratos");
	}
}
