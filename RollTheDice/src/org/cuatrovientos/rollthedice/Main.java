/**
 * 
 */
package org.cuatrovientos.rollthedice;

/**
 * @author david_madurga
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Dice MyDice=new Dice();
		for(int i =0;i <10;i++){
		System.out.println(MyDice.roll());
		}
		Dice d20=new Dice(20);
		for(int i =0;i <10;i++){
		System.out.println(d20.roll());
		}
		
		
		Player anotherPlayer= new Player(6,5,7,100,8,"Jefe");
		Warrior fantasmaDeSparta = new Warrior();
		
		System.out.println("Name: " + fantasmaDeSparta.getName() );
		System.out.println("Strength: " + fantasmaDeSparta.getStrength() );
		System.out.println("Inteligence: " + fantasmaDeSparta.getInteligence() );
		System.out.println("Resistance: " + fantasmaDeSparta.getResistance() );
		System.out.println("Speed: " + fantasmaDeSparta.getSpeed() );
		System.out.println("Lifepoints: " + fantasmaDeSparta.getLifepoints() );
		
		System.out.println();
		System.out.println("Name: " + anotherPlayer.getName() );
		System.out.println("Strength: " + anotherPlayer.getStrength() );
		System.out.println("Inteligence: " + anotherPlayer.getInteligence() );
		System.out.println("Resistance: " + anotherPlayer.getResistance() );
		System.out.println("Speed: " + anotherPlayer.getSpeed() );
		System.out.println("Lifepoints: " + anotherPlayer.getLifepoints() );
		
		
	}

}
