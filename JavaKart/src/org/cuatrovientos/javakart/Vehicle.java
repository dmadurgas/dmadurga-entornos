/**
 * 
 */
package org.cuatrovientos.javakart;

import java.util.Random;

/**
 * the father of car
 * @author david_madurga
 *
 */
public class Vehicle {
	protected Random rand = new Random();
	protected String name="";
	protected int speed=0;
	protected int acelerate=0;
	protected int take=0;
	
	/**
	 * Constructor
	 * @param name
	 */
	public Vehicle (String name){
		this.name=name;
		init();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * @return the acelerate
	 */
	public int getAcelerate() {
		return acelerate;
	}

	/**
	 * @param acelerate the acelerate to set
	 */
	public void setAcelerate(int acelerate) {
		this.acelerate = acelerate;
	}

	/**
	 * @return the take
	 */
	public int getStart() {
		return take;
	}

	/**
	 * @param take the start to set
	 */
	public void setStart(int take) {
		this.take = take;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Vehicle [name=" + name + ", speed=" + speed + ", acelerate="
				+ acelerate + ", take=" + take + "]";
	}
	/**
	 * inicializate values
	 */
	private void init() {
		for (int i = 0; i< 18; i++){
			
			switch(rand.nextInt(3)){
			
				case 0:
					speed++;
					break;
				case 1:
					acelerate++;
					break;
				case 2:
					take++;
					break;
			}
		}
		
	}
	
	/**
	 * moving the vehicle
	 * @return
	 */
	public int move () {
		
		return speed + acelerate + rand.nextInt(6);
		
	}
	/**
	 * manoeuvre the vehicle
	 * @return
	 */
	public int manoeuvre(){
		
		return take+rand.nextInt(6);
	}
	
}
