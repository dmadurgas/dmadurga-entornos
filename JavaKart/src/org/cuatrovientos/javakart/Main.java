/**
 * 
 */
package org.cuatrovientos.javakart;

/**
 * @author david_madurga
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car myCar= new Car("Ferrarri","12345AD");
		Car otherCar= new Car("PapaMovil","66666DD");
		Car theCar= new Car("Delorean","2050DC");
		Car fallCar = new Car("Jofrey Baratheon", "67329ADS");
		
		Circuit myCircuit = new Circuit("Death Star", 100);
		Race myRace = new Race("Westeros", myCircuit);
		myRace.insertCar(myCar);
		myRace.insertCar(otherCar);
		myRace.insertCar(theCar);
		myRace.insertCar(fallCar);
	
		System.out.println("The winner in " + myRace.getName() + " " + myCircuit.getName() +  " is :" + myRace.run().toString());
		
	}

}
