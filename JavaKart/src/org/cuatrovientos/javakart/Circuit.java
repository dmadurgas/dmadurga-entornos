/**
 * 
 */
package org.cuatrovientos.javakart;

/**
 * which run the car
 * @author david_madurga
 *
 */
public class Circuit {

	private String name;
	private int kilometres;
	
	/**
	 * Constructor
	 * @param name
	 * @param kilometres
	 */
	public Circuit(String name, int kilometres) {
		this.name = name;
		this.kilometres = kilometres;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the kilometres
	 */
	public int getKilometres() {
		return kilometres;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Circuit [name=" + name + ", pole=" + kilometres + "]";
	}
	
	
	
}
