/**
 * 
 */
package org.cuatrovientos.javakart;

/**
 * It run in circuit in race
 * @author david_madurga
 *
 */
public class Car extends Vehicle {
	
	private String number;
	
	/**
	 * Constructor
	 * @param name
	 * @param number
	 */
	public Car (String name, String number) {
		super(name);
		this.number=number;
	}
	

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}


	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Car [number=" + number + ", name=" + name + ", speed=" + speed
				+ ", acelerate=" + acelerate + ", take=" + take + "]";
	}

	
	
	
}
