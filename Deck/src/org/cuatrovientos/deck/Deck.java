/**
 * 
 */
package org.cuatrovientos.deck;

import java.util.Collections;
import java.util.Vector;

/**
 * is a deck whit cards 
 * @author david_madurga
 *
 */
public class Deck {
	
	public Vector<Card> mycards;
	/**
	 * @param mycards
	 */
	public Deck() {
		this.mycards = new Vector<Card>();
		loadDeck(0,0,2);
	}
/**
 * REcursive function to load Deck with cards
 * @param figures
 * @param cont
 * @param num1
 * @param values
 */
	private void loadDeck( int cont, int num1, int values){
		
		String[] name={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K","A"};
		String[] suit={"Spades","Clubs","Hearts","Diamonds"};
		
		do{
				mycards.add(new Card(name[cont],suit[num1],values++));
				cont++;
			}while (cont<13);
			num1++;
			cont=0;
			values=2;
		if (mycards.size()<52)
		loadDeck( cont, num1, values);
	}
	
	public void shuffle(){
		Collections.shuffle(mycards);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Deck [mycards=" + mycards + "]";
	}
	
	public Card getCard(int value){
		return mycards.get(value);
	}


}
