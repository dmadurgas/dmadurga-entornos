/**
 * 
 */
package org.cuatrovientos.deck;

/**
 * is a Card
 * @author david_madurga
 *
 */
public class Card {
	private String name;
	private String suit;
	private int value;
	
	/**
	 * Constructor
	 * @param name
	 * @param element
	 * @param value
	 */
	public Card(String name, String suit, int value) {
		this.name = name;
		this.suit = suit;
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the element
	 */
	public String getSuit() {
		return suit;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Card [name=" + name + ", suit=" + suit + ", value="
				+ value + "]";
	}
	
	
	
	
	
}
