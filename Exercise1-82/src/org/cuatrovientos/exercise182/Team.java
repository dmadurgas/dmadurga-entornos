/**
 * 
 */
package org.cuatrovientos.exercise182;
import java.util.Vector;

/**
 * The Team
 * @author david_madurga
 *
 */
public class Team extends Vector<Player> {

	
	private String name;
	private int foundation;
	private double cost;
	//private Vector<Player>myPlayer=new Vector<Player>();
	
	/**
	 * Constructor
	 * @param name
	 * @param foundation
	 * @param cost
	 */
	public Team(String name, int foundation, double cost) {
		this.name = name;
		this.foundation = foundation;
		this.cost = cost;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Team [name=" + name + ", foundation=" + foundation + ", cost="
				+ cost + "]";
	}
	
	/**
	 * @return the myPlayer
	 */
	//public Vector<Player> getMyPlayer() {
		//return myPlayer;
	//}
	/**
	 * Insert player
	 * @param myPlayer
	 */
	//public void insert(Player myPlayer){
		//this.myPlayer.add(myPlayer);
	//}
	
	
	
}
