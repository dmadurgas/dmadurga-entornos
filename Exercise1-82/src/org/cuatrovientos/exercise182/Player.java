/**
 * 
 */
package org.cuatrovientos.exercise182;

/**
 * the player
 * @author david_madurga
 *
 */
public class Player {

	private String name;
	private String pole;
	private int number;
	
	/**
	 * Constructor
	 * @param name
	 * @param pole
	 * @param number
	 */
	public Player(String name, String pole, int number) {
		this.name = name;
		this.pole = pole;
		this.number = number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Player [name=" + name + ", pole=" + pole + ", number=" + number
				+ "]";
	}
	
	
	
	
}
