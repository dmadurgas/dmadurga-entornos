/**
 * 
 */
package org.cuatrovientos.exercise182;

/**
 * @author david_madurga
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		Team myTeam=new Team("Dream Team",1000,289.90d);
		Player myPlayer = new Player("Dark Vader", "centre",1);
		Player otherPlayer = new Player("Ivernalia", "subcentre",50);
		Player thePlayer = new Player("Jofrey", "centre",12);
		
		
		myTeam.add(myPlayer);
		myTeam.add(otherPlayer);
		myTeam.add(thePlayer);
		
		for(Player p: myTeam){
			System.out.println(p.toString());
		}
	}

}
