/**
 * 
 */
package org.cuatrovientos.arrays;

/**
 * example of array
 * @author david_madurga
 *
 */
public class Arrays {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int oneNumber = 5;
		int[] manyNumbers = {15, 42, 128, 0, 5};
		String[] heroes = {"Gimli", "Boromir", "Frodo"};
		
		manyNumbers[3] = oneNumber;
		manyNumbers[0] = 666;
		
		for (int i = 0; i < manyNumbers.length; i++) {
			
			System.out.println(i+ ". " + manyNumbers[i]);
			
		}
		heroes[2]= "Hodor";
		for (int i = 0; i < heroes.length; i++) {
			System.out.println(i+ ". " + heroes[i]);
		}
	}

}
