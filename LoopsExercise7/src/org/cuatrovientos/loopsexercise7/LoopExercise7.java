/**
 * 
 */
package org.cuatrovientos.loopsexercise7;


/**
 * @author david_madurga
 *
 */
public class LoopExercise7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			
				
		for (int i = 0; i<=10; i++) {
			
			for (int j = 0; j<=10; j++) {
				System.out.println(i + " x " + j + " = " + i*j);		
			}
			
			System.out.println("");

		}
		
		
		
		
						
	}

}
