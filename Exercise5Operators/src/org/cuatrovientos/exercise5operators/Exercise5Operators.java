/**
 * 
 */
package org.cuatrovientos.exercise5operators;

import java.util.Scanner;

/**
 * @title Exercise 5 Operators 
 * @author david_madurga
 * @version 1.0
 */
public class Exercise5Operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String numberString = "";
		int numberInteger = 0;
		float numberResult = 0F;
		
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter the first number: ");
		numberString = reader.nextLine();
		numberInteger+= Integer.parseInt(numberString);/*conversion*/
		
		System.out.print("Enter the second number: ");
		numberString = reader.nextLine();
		numberInteger= numberInteger+ Integer.parseInt(numberString);/*conversion*/
		
		System.out.print("Enter the third number: ");
		numberString = reader.nextLine();
		numberInteger= numberInteger+ Integer.parseInt(numberString);/*conversion*/
		
		System.out.print("Enter the four number: ");
		numberString = reader.nextLine();
		numberInteger= numberInteger+ Integer.parseInt(numberString);/*conversion*/
		
		System.out.print("Enter the five number: ");
		numberString = reader.nextLine();
		numberInteger= numberInteger+ Integer.parseInt(numberString);/*conversion*/
		
		numberResult= numberInteger /5;
		System.out.println("The average is: " + numberResult);
		reader.close();
		
		
		
				
		

	}

}
