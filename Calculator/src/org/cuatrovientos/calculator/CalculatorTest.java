/**
 * 
 */
package org.cuatrovientos.calculator;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class CalculatorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

		public Calculator mycalculator = new Calculator();
		
	/**
	 * Test method for {@link org.cuatrovientos.calculator.Calculator#add(int, int)}.
	 */
	@Test
	public void testAdd() {
		int target=mycalculator.add(5, 5);
		int expected=10;
		assertEquals("ADD test",expected,target);
	}

	/**
	 * Test method for {@link org.cuatrovientos.calculator.Calculator#mul(int, int)}.
	 */
	@Test
	public void testMul() {
		int target=mycalculator.mul(5, 5);
		int expected=25;
		assertEquals("mul test",expected,target);
	}

	/**
	 * Test method for {@link org.cuatrovientos.calculator.Calculator#sub(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSub() {
		float target=mycalculator.sub(5, 5);
		float expected=1;
		assertEquals("sub test",expected,target,0.0f);
	}

}
