/**
 * 
 */
package org.cuatrovientos.calculator;


import java.util.Scanner;
/**
 * make a calculator
 * @author david_madurga
 *
 */
public class Calculator {

	/**add to numbers and return result 
	 * @param arg1
	 * @param arg2
	 * @return result
	 */
	
	public  int add(int arg1, int arg2) {
		int result;
		result= arg1+arg2;
		return result;
	}
	/**mul to numbers and return result 
	 * @param arg1
	 * @param arg2
	 * @return result
	 */
	public int mul(int arg1, int arg2) {
		int result;
		result= arg1*arg2;
		return result;
	}
	
	/** substracts to numbers and return result 
	 * @param arg1
	 * @param arg2
	 * @return result
	 */
		public  float sub(int arg1, int arg2) {
		float result;
		result= (float) (arg1/arg2);
		return result;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String line = "";
		String line1 = "";
		int number=0;
		int number1=0;
		Calculator mycalculator = new Calculator();
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter a number ");
		line= reader.nextLine();
		
		number=Integer.parseInt(line);
		
		System.out.print("Enter a number ");
		line1= reader.nextLine();
		
		number1=Integer.parseInt(line1);
		
		System.out.println(mycalculator.add(number, number1));
		System.out.println(mycalculator.mul(number, number1));
		System.out.println(mycalculator.sub(Integer.parseInt(line),Integer.parseInt(line1)));
		
		reader.close();
		
		
		

	}

}
