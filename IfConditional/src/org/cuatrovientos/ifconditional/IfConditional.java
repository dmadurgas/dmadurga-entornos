/**
 * 
 */
package org.cuatrovientos.ifconditional;

import java.util.Scanner;

/**
 * Simple class to test conditional structures
 * @author david_madurga
 * @version 1
 */
public class IfConditional {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int number = 0;
		String line = ""; /*to store what i read from console*/
		
		//Special object to read from console
		Scanner reader = new Scanner(System.in);
		
		System.out.print("Enter a number ");
		line = reader.nextLine();
		number = Integer.parseInt(line);
		
		/*if ((line)) {
			number = Integer.parseInt(line);
		} else {
			System.out.print("Enter a number ");
			line = reader.nextLine();
		}*/
		
		if (number >0) {
			System.out.println(number +" is bigger than 0");
		} else {
			if (number<0) {
				System.out.println(number +" is negative");
			} else {
				System.out.println(number +" is 0");
			}
		}
		reader.close();
		
		

	}

}
