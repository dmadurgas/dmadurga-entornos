/**
 * 
 */
package org.cuatrovientos.arraysexercise2;

import java.util.Scanner;

/**
 * ArraysExercise2
 * @author david_madurga
 *
 */
public class ArraysExercise2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String [] line = new String[10];
		Scanner reader = new Scanner (System.in);
		
		
		for (int i=0; i<10; i++) {
			System.out.println("Entre a " + i + " Element");
			line[i]=reader.nextLine();
		}
		
		for (int i=0; i<10; i++) {
			System.out.println(line[i]);
		}
		
		
		reader.close();
		
		
	}

}
