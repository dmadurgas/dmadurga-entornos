/**
 * 
 */
package org.cuatrovientos.arraysexercise8;

import java.util.Random;

/**
 * @author david_madurga
 *
 */
public class ArraysExercise8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int [][] number= new int[5][10];
		Random rnd = new Random();
		
		for (int i= 0; i < number.length;i++) {
			for (int j = 0; j < number[i].length;j++ ) {
				number[i][j]=rnd.nextInt(30);
			}			
		}
		salida:
		for (int i= 0; i < number.length;i++) {
			for (int j = 0; j < number[i].length;j++ ) {
				if (number[i][j] == 15){
					System.out.println("The number is in " + i + " x" + " and " + j + " y");
					break salida;
				}
			}			
		}
		
		
	}

}
