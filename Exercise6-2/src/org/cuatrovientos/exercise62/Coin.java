/**
 * 
 */
package org.cuatrovientos.exercise62;

/**
 * @author david_madurga
 *
 */
public class Coin {

	
	/**
	 * Empty Constructor
	 */
	public Coin(){
		
	}
	
	/**
	 * Flip the coin
	 * @return flip
	 */
	public String flip () {
		String flip="";
		int option =0;
		option=(int) Math.round((Math.random()*2+0));
		switch (option)  {
		case 0:
			flip="Head";
			break;
		case 1:
			flip="Cross";
			break;
		default:
			flip="Edge";
			break;
		}
		return flip;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Coin myCoin = new Coin();
		
		System.out.println(myCoin.flip());

	}

}
