/**
 * 
 */
package org.cuatrovientos.arraysexercise1;

/**
 * Arrys Exercise 1
 * @author david_madurga
 *
 */
public class ArraysExercise1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
    
		int [] number = {8,88,7,2,1,3,6,5,0,13};
		for (int i=0;i<10;i++) {
			System.out.println(number[i]);
			
		}
		
	}

}
