/**
 * 
 */
package org.cuatrovientos.loopsexercise1;

import java.util.Scanner;

/**
 * @author david_madurga
 *
 */
public class LoopsExercise1 {

	/**
	 * 
	 * exercise 1 loop
	 * @param args
	 */
	public static void main(String[] args) {
		String line = "";
		int number = 0;
		Scanner reader= new Scanner(System.in);
		
		
		System.out.println("Enter a number");
		line=reader.nextLine();
		number = Integer.parseInt(line);
		
		if (number > 0) {
			for (int i = 0; i < number; i++) {
				System.out.println("hello boy");
			}
			
		} else {
			System.out.println("The number isn�t bigger than 0, idiot");
		}
		reader.close();
		return;
		
		
	}

}
