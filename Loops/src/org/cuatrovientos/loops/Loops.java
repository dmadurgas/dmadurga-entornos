/**
 * 
 */
package org.cuatrovientos.loops;


import java.util.Scanner;

/**
 * example for loop
 * @author david_madurga
 * @version 1
 *
 */
public class Loops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int counter = 10;
		String message = "FAP";
		String line = "";
		Scanner reader = new Scanner(System.in);
		int number = 0;
		
		while(counter > 0) {
			System.out.println(message);
			counter--;
		}
	
		

		do {
			System.out.println("The number of the beast is: ");
			line=reader.nextLine();
			number = Integer.parseInt(line);
			
		} while (number != 666);
		
		System.out.println("Hell yeah!");
		
		for (int i =0; i < 666; i++){
			System.out.println(i + " Satan ");
		}
			reader.close();
		
	
	}
	

}
