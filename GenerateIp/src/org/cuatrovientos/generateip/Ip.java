/**
 * 
 */
package org.cuatrovientos.generateip;

/**
 * @author david_madurga
 *
 */
public class Ip {

	/**
	 * 
	 * @param i
	 * @param j
	 * @return Int
	 */
	public int generateNumber(int i, int j) {
		
		return (int) (Math.random()*j-i);
	}

	/***
	 * 
	 * @return String
	 */
	public String generateIp() {
		int number;
		String result="";
		for (int i=0;i<3;i++){
			number=generateNumber(0,254);
			if (i==0||i==2){
				if (number==0){
					number=generateNumber(0,254);
				}
			}
			result+=number+".";
			
		}
		return result.substring(0, result.length()-1);
		
		 
	}

}
