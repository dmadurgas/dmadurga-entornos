/**
 * 
 */
package org.cuatrovientos.generateip;

//import static org.junit.Assert.*;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import junit.extensions.RepeatedTest;

import org.junit.Before;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class GenerateIpTest  extends TestCase{

	/**
	 * @throws java.lang.Exception
	 */
	public Ip oneIp;//=new Ip();
	
	/***
	 * Constructor 
	 * @param testMethodName
	 */
	public GenerateIpTest(String testMethodName) {
		super(testMethodName);
	}

	
	@Before
	public void setUp() throws Exception {
	oneIp=new Ip();
	}
	
	/***
	 * Test method GenerateIp
	 */
	@Test
	public void testGenerateNumber() {
		int target=oneIp.generateNumber(0,254);
		assertTrue("Test generate random number", target<=254 && target>=0);
	}
	
	/***
	 * Test method GenerateIp;
	 */
	@Test
	public void testGenerateIp(){
		String target=oneIp.generateIp();
		assertFalse("Test generate ip",target.startsWith("0") && target.endsWith("0"));
	}
	
	@Test
	/**
	 * You must use to TestSuite
	 * @return
	 */
	public static TestSuite suite(){
		TestSuite  suite = new TestSuite();
		suite.addTest(new RepeatedTest(new GenerateIpTest("testGenerateNumber"),1000));
		suite.addTest(new GenerateIpTest("testGenerateIp"));
		return  suite;
		
	}

}
