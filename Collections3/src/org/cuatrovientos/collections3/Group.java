/**
 * 
 */
package org.cuatrovientos.collections3;

import java.util.Hashtable;

/**
 * Contains Alum
 * @author david_madurga
 *
 */
public class Group extends Hashtable<String,Alumn> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;

	/**
	 * Constructor
	 * @param name
	 */
	public Group(String name) {
		this.name = name;
	}

	/**
	 * Get Name
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set Name
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
