/**
 * 
 */
package org.cuatrovientos.collections3;

/**
 * Is a Alumn
 * @author david_madurga
 *
 */
public class Alumn {

	private String id;
	private String name;
	private String course;
	/**
	 * Constructor
	 * @param id
	 * @param name
	 * @param course
	 */
	public Alumn(String id, String name, String course) {
		this.id = id;
		this.name = name;
		this.course = course;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Alumn [id=" + id + ", name=" + name + ", course=" + course
				+ "]";
	}
	
	
	
}
