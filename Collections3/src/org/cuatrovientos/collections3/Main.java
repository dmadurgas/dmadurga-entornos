/**
 * 
 */
package org.cuatrovientos.collections3;

import java.util.Enumeration;

/**
 * @author david_madurga
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Group myGroup=new Group("Westeros");
		Alumn myAlumn= new Alumn("11111111F","Tyron Lancaster","miio");
		Alumn otherAlumn= new Alumn("2222222F","Gandalf","miio");
		
		myGroup.put(myAlumn.getId(), myAlumn);
		myGroup.put(otherAlumn.getId(),otherAlumn);
		myGroup.put("3333333J", new Alumn("3333333J", "pokemon", "fgh"));
		
		Enumeration<String> list = myGroup.keys();
		
		while (list.hasMoreElements()){
			String key = (String)list.nextElement();
			System.out.println(myGroup.get(key).toString());
		}
	}

}
