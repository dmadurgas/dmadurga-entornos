package org.cuatrovientos.exercise4class;

import java.util.Scanner;

public class Array {
	
	private int[] numbers=new int[10];
	/**
	 * start array
	 */
	public void start() {
		Scanner reader = new Scanner(System.in);
		String line="";
		for (int i = 0; i < 10; i++){
				
			System.out.print("Enter a "+ i++ + "number: ");
			line=reader.nextLine();
			
			this.numbers[i]=Integer.parseInt(line);
			
		}
		reader.close();
	}
	/**
	 * increment array values
	 */
	public void increment () {
		for (int i = 0; i < 10; i++){
			this.numbers[i]++;
			
		}
	}
	/**
	 * decrement array values
	 */
	public void decrement () {
		for (int i = 0; i < 10; i++){
			this.numbers[i]--;
			
		}		
	}
	/**
	 * show pair array values 
	 */
	public void pair () {
		for (int i = 0; i < 10; i++){
			if(numbers[i]%2==0){
				System.out.println(numbers[i]);
			}
			
		}
		
	}
}
