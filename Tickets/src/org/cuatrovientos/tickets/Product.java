/**
 * 
 */
package org.cuatrovientos.tickets;

/**
 * represent a Product
 * @author David
 *
 */
public class Product {

	private String name;
	private float prize;
	private int cuantity;
	private float total;
	public Product(String name, float prize, int quantity) {
		this.name=name;
		this.prize=prize;
		this.cuantity=quantity;
		this.total=quantity*prize;
	}

	public String getName() {
		
		return name;
	}

	public float getPrize(){
		return prize;
	}

	public int getQuantity() {
		return cuantity ;
	}

	public float getTotal() {
		
		return total;
	}
	

}
