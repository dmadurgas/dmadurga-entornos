package org.cuatrovientos.tickets;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {//extends TestCase {

	@Test
		public void newTest(){
			Product oneProduct = new Product("bread",0.50f,1);
			assertEquals("Test name","bread",oneProduct.getName());
	
			assertTrue("Test cost",0.50f==oneProduct.getPrize());
			assertSame("Test quantity", 1,oneProduct.getQuantity());
		}
	@Test
	public void totalTest(){
		Product oneProduct = new Product("bread",0.50f,2);
		assertTrue("Test Total", 1.00f==oneProduct.getTotal());
	}

}
