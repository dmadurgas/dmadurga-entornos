/**
 * 
 */
package org.cuatrovientos.tickets;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author David
 *
 */
public class TicketTest {

	private static final float IVA = 0.21f;
	private static final float TOTAL_TICKET = 0.50f*2+0.90f*3+0.10f*3;
	public Product[] products = {new Product("bread",0.50f,2),new Product("chesse",0.90f,3), new Product("tomatoes",0.10f,3)}; 
	public Ticket oneTicket= new Ticket();
	@Before
	public void setUp(){

		oneTicket.add(products[0]);
		oneTicket.add(products[1]);
		oneTicket.add(products[2]);
	}
	
	@Test
	public void totalTicketTest() {
		float expected=TOTAL_TICKET;
		float target= oneTicket.getTotal();
		assertEquals("Test Total",expected,target,0.0f);
	}

	@Test
	public void totalIvaTicketTest() {
		
		float expected=(TOTAL_TICKET+TOTAL_TICKET*IVA);
		float target=oneTicket.getTotalIva();
		oneTicket.getTotal();
		assertEquals("Test total iva",expected,target,0.0f);
		
		
	}
		
	
}
