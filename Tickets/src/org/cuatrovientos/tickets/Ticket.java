/**
 * 
 */
package org.cuatrovientos.tickets;

import java.util.Vector;

/**
 * represent at Ticket
 * @author David
 *
 */
public class Ticket extends Vector<Product>{

	private static final float IVA = 0.21f;
	private static final long serialVersionUID = -5230046357775576299L;
	private float total;
	
	/***
	 * Calculate Total ticket
	 * @return total
	 */
	public float getTotal() {
		
		for (int i =0;i<this.size();i++){
			total+=this.elementAt(i).getTotal();
		}
		return total;
	}

	/***
	 * Calculate total with iva
	 * @return float
	 */
	public float getTotalIva() {
		return total+total*IVA;
	}

}
