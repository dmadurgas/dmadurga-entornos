/**
 * 
 */
package org.cuatrovientos.generador;

import java.util.Random;
/**
 * @author david_madurga
 *
 */
public class Generador {

	/**
	 * generate password
	 * @param longitud
	 * @return password
	 */
	
	public static String makePassword(int longitud) {
		
		String password1="abcdefghijclm�opqrstuvwxyz0123456789AQZWSXEDCRFVTGBYHNUJMIKLOP�-_";
		String password="";
		Random random =new Random();
				
		for(int i=0; i< longitud;i++) {
			password+= password1.charAt(random.nextInt(password1.length()));
		}
		
		return password;
	}
	
	/**
	 * generate a namber
	 * @param letters
	 * @return password
	 */
	public static String generateName(int letters) {
		String password="";
		String[] vocal={"a","e","i","o","u"};
		String consonant="qzwsxdcrfvtgbyhnjmkl�p";
		Random random =new Random();
		boolean inter=false;
		
		for (int i=0 ; i< letters; i++ ) {
			if(inter==false){
			password+=vocal[random.nextInt(5)];
					inter=true;
			}else{
				password+= consonant.charAt(random.nextInt(consonant.length()));
				inter=false;
			}
		}
		
		
		return password;
	}
	
	/**
	 * generate elf name
	 * @param letters
	 * @return password
	 */
	public static String generateNameElf(int letters){
		String password="";
		String[] vocal={"a","e","i","o","u"};
		String consonant="qzwsxdcrfvtgbyhnjmkl�p";
		Random random =new Random();
		boolean inter=false;
		
		for (int i= 0; i < letters; i++ ) {
			
			if(inter==false) {
				password+=vocal[random.nextInt(5)];
				inter=true;
			}else{
				password+= consonant.charAt(random.nextInt(consonant.length()));
				inter=false;
			}
		}
		
		switch (random.nextInt(6)) {
		case 0:
			password= password.substring(0, password.length()-2)+ "as";
			break;
		case 1:
			password= password.substring(0, password.length()-2)+ "il";
			break;
		case 2:
			password= password.substring(0, password.length()-3)+ "fin";
			break;
		case 3:
			password= password.substring(0, password.length()-2)+ "as";
			break;
		case 4:
			password= password.substring(0, password.length()-2)+ "en";
			break;
		case 5:
			password= password.substring(0, password.length()-2)+ "in";
			break;
		}
		return password;
			
	}
	
	public static String generateNameOrc(int letters){
		String password="";
		String[] vocal={"a","e","i","o","u"};
		String consonant="qzwsxdcrfvtgbyhnjmkl�p";
		Random random =new Random();
		boolean inter=false;
		
		for (int i= 0; i < letters; i++ ) {
			
			if(inter==false) {
				password+=vocal[random.nextInt(5)];
				inter=true;
			}else{
				password+= consonant.charAt(random.nextInt(consonant.length()));
				inter=false;
			}
		}
		
		switch (random.nextInt(6)) {
		case 0:
			password= password.substring(0, password.length()-2)+ "ur";
			break;
		case 1:
			password= password.substring(0, password.length()-2)+ "or";
			break;
		case 2:
			password= password.substring(0, password.length()-3)+ "oth";
			break;
		case 3:
			password= password.substring(0, password.length()-2)+ "ug";
			break;
		case 4:
			password= password.substring(0, password.length()-2)+ "og";
			break;
		case 5:
			password= password.substring(0, password.length()-3)+ "mog";
			break;
		}
		return password;
			
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
			
		
		
		
		System.out.println(Generador.makePassword(10));
		System.out.println(Generador.generateName(10));
		System.out.println(Generador.generateNameElf(4));
		System.out.println(Generador.generateNameOrc(4));
	}

}
