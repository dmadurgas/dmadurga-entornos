package org.cuatrovientos.empirefleet;

/**
 * Represents a Tie Bomber
 * @author Sienar Fleet Systems
 * @greets the emperor
 */
public class TieBomber  extends SpaceShip {
	// attack range
	
	private int bombs;

	/**
	 * constructor
	 * @param n
	 * @param type 
	 */
	public TieBomber (String n, int type) {
		super(0);
		pilotName = n;
		this.type=type;
	}

	/**
	 * only for bomber type (==2)
	 * drop a bomb
	 * @return bomb damage
	 */
	public int bomb() {
		if (getType() == 2)
			return 5;
		else
			return 0;
	}

	int getBombs() {
		return bombs;
	}

	void setBombs(int bombs) {
		this.bombs = bombs;
	}

	/**
	 * "I'm firing my laser"
	 * @return laser damage depending of type
	 */
	public int fireLaser() {
		int damage = 0;
		switch (getType()) {
			case 0: damage = 1;
					break;
			case 1: damage = 2;
					break;
			case 2: damage = 3;
					break;
			default:damage = 0;
					break;
		}
		
		return damage;
	}

	/**
	 * move, more or less depending on type
	 * @return positions
	 */
	public int move() {
		int movement = 0;
		switch (getType()) {
			case 0: movement = 1;
					break;
			case 1: movement = 3;
					break;
			case 2: movement = 2;
					break;
			default:movement = 0;
					break;
		}
		
		return movement;
	}
}
