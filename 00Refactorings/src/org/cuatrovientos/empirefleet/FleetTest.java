package org.cuatrovientos.empirefleet;

import static org.junit.Assert.*;

import org.junit.Test;

public class FleetTest {

	/***
	 * Test empty fleet
	 */
	@Test
	public void constructorTest() {
		Fleet oneFleet = new Fleet();
		assertTrue("Fleet is nothing",oneFleet.isEmpty());
		
	}

	@Test
	public void addFleetTest(){
		Fleet oneFleet = new Fleet();
		oneFleet.setCommanderName("dark vader");
		for(int i =0;i< 5; i++ ){
			oneFleet.setFleet(3, "victim");
		}
		assertTrue("Contains 5 elements",5==oneFleet.size());
	}
}
