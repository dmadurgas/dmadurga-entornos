package org.cuatrovientos.empirefleet;

/**
 * Represents a Tie Fighter starfighter
 * @author Sienar Fleet Systems
 * @greets the emperor
 */
public class TieFighter extends SpaceShip {
	// attack range
	
	/**
	 * constructor
	 * @param n
	 * @param type 
	 */
	public TieFighter (String n, int type) {
		super(0);
		pilotName = n;
		this.type=type;
	}

	/**
	 * "I'm firing my laser"
	 * @return laser damage depending of type
	 */
	public int fireLaser() {
		int damage = 0;
		switch (getType()) {
			case 0: damage = 1;
					break;
			case 1: damage = 2;
					break;
			case 2: damage = 3;
					break;
			default:damage = 0;
					break;
		}
		
		return damage;
	}

	/**
	 * move, more or less depending on type
	 * @return positions
	 */
	public int move() {
		int movement = 0;
		switch (getType()) {
			case 0: movement = 1;
					break;
			case 1: movement = 3;
					break;
			case 2: movement = 2;
					break;
			default:movement = 0;
					break;
		}
		
		return movement;
	}
}
