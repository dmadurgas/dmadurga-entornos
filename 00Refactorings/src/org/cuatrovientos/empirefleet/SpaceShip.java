package org.cuatrovientos.empirefleet;

/**
 * 	Represents a generic space_ship
 * @author Sienar Fleet Systems
 * @greets the emperor
 */
public class SpaceShip {
	protected int type;
	private int speed;
	protected int attackRange;
	protected int energy;
	protected String pilotName;
	Position coord = new Position();
	
	/**
	 * constructor
	 * @param type
	 */
	public SpaceShip (int type)  {
		setType(type);
	}
	
	int getSpeed() {
		return speed;
	}

	void setSpeed(int speed) {
		this.speed = speed;
	}

	protected int getType() {
		return type;
	}

	void setType(int type) {
		this.type = type;
	}

	/**
	 * only tie advanced and bombers
	 * fires ION cannon
	 * @return damage
	 */
	public int fireIonCannon () {
		if (getType()==1 || getType()==2) 
			return 3;
		else 
			return 0;
	}
	
	String getPilotName() {
		return pilotName;
	}

	protected void setPilotName(String pilotName) {
		this.pilotName = pilotName;
	}

	int getEnergy() {
		return energy;
	}

	void setEnergy(int energy) {
		this.energy = energy;
	}

	int getAttackRange() {
		return attackRange;
	}

	void setAttackRange(int attackRange) {
		this.attackRange = attackRange;
	}
}
