package org.cuatrovientos.empirefleet;

import java.util.Scanner;
import java.util.Vector;

/**
 * Represents an imperial fleet composed
 * of many space_ships
 * @author Sienar Fleet Systems
 * @greets the emperor
 */
public class Fleet extends Vector<SpaceShip> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String commanderName;
	
	String getCommanderName() {
		return commanderName;
	}

	void setCommanderName(String commanderName) {
		this.commanderName = commanderName;
	}

	/**
	 * creates fleet
	 * three steps: define commander name, create fleet, show fleet
	 */
	public void create_fleet () {
		Scanner reader = new Scanner(System.in);
		newCommanderName(reader);
	}

	public void resume() {
		for (SpaceShip ship : this) {
			System.out.println("Type: " + ship.getType() + "," + ship.toString());
		}
	}

	public void setFleet(int type,String pilot) {
		
			
			switch (type) {
				case 1: // a tie fighter
						this.add(new TieFighter(pilot,type));
						break;
				case 2:
						this.add(new TieAvanced(pilot,type));
						break;
				case 3:
						this.add(new TieBomber(pilot,type));
						break;
				default:
				
			}
			
	}
	

	private void newCommanderName(Scanner reader) {
		do {
			System.out.println("Identify yourself");
			setCommanderName(reader.nextLine());
		} while (getCommanderName().isEmpty());
	}
}
