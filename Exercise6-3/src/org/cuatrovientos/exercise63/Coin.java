/**
 * 
 */
package org.cuatrovientos.exercise63;

/**
 * @author david_madurga
 *
 */
public class Coin {

	
	
	
	private static final double CHANGE_EURO_DOLAR = 1.3d;

	public double ptsToEuro(double thing) {
		double conv = 166.386d;
		return conv/thing;
	}
	
	public double euroToDolar(double thing) {
		
		return CHANGE_EURO_DOLAR*thing;
	}
	
	public double dolarToEuro(double thing) {
		
		return thing/1.3d;
	}
	
	public double euroToPts(double thing) {
		
		return thing*166.386d;
	}


	public double euroToPound(double thing) {
	
		return thing*0.386d;
	}


	public double poundToEuro(double thing) {
	
		return thing*101d;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
