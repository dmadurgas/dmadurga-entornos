/**
 * 
 */
package org.cuatrovientos.exercise1operators;

/**
 * @title Exercise 1 Operators
 * @author david_madurga
 * @version 1.0
 */
public class Exercise1Operators {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int resulInt= 0;
		float resulFloat= 0F;
		resulInt = 10 - 7;
		System.out.println("10-7= " + resulInt );
		resulInt = 40 + 2;
		System.out.println("40+2= " + resulInt );
		resulInt = 10 * 4 + 2;
		System.out.println("10*4+2= " + resulInt );
		resulInt = 56 % 4;
		System.out.println("50%4= " + resulInt );
		resulFloat = (100 / 4) + 25 - (16 / 2);
		System.out.println("(100/4)+25-(16/2)= " + resulFloat );
		resulFloat = 39.56F + 3 * 100;
		System.out.println("39.56F+3*100= " + resulFloat );
		resulFloat = (2 + 1) + 5 /3;
		System.out.println("(2++)+5/3= " + resulFloat );
	}

}
