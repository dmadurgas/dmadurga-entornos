/**
 * 
 */
package org.cuatrovientos.greetinterfaces;



/**
 * @author david_madurga
 *
 */
public interface Greet {

	public String greet();
}
