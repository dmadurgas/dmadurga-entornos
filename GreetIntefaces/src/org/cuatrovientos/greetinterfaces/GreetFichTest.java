/**
 * 
 */
package org.cuatrovientos.greetinterfaces;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author david_madurga
 *
 */
public class GreetFichTest {

	
	public GreetFich myfich;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		 myfich = new GreetFich();
	}

	/**
	 * Test method for {@link org.cuatrovientos.greetinterfaces.GreetFich#getNomFichero()}.
	 */
	@Test
	public void testGetNomFichero() {
		myfich.setNomFichero("greet.txt");
		String target = myfich.getNomFichero();
		String expected = "greet.txt";
		
		assertEquals("Test Get Name",expected,target);
	}

		/**
	 * Test method for {@link org.cuatrovientos.greetinterfaces.GreetFich#greet()}.
	 */
	@Test
	public void testGreet() {
		myfich.setNomFichero("greet.txt");
		String expected= "hi";
		try {
			assertEquals("Test read file", expected,myfich.greet());
		}
		catch (Exception ex) {	
		}
	}

}
