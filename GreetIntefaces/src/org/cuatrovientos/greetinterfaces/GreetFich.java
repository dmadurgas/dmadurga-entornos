/**
 * 
 */
package org.cuatrovientos.greetinterfaces;
import java.io.*;

/**
 * @author david_madurga
 *
 */
public class GreetFich implements Greet {

	private String nomFichero;
	private File fichero;
	
	/**
	 * @return the nomFichero
	 */
	public String getNomFichero() {
		return nomFichero;
	}

	/**
	 * @param nomFichero the nomFichero to set
	 */
	public void setNomFichero(String nomFichero) {
		this.nomFichero = nomFichero;
	}


	
	

	@Override
	public String greet() {
		String line = "";
		try {
			fichero= new File(nomFichero);
			FileReader reader = new FileReader(fichero);
			BufferedReader bufferReader = new BufferedReader(reader);
			line= bufferReader.readLine();
			bufferReader.close();
			return line;
		}
		catch (IOException ioe){
			return "Error of read " + ioe.getMessage();
		}
		
		
	}

}
