/**
 * 
 */
package org.cuatrovientos.listadelacompra;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 *
 * @author root
 */
public class ListaDeLaCompra {

/**
 * show the menu
 */
	public static void mostrarMenu() {
		
		System.out.println("Menu");
        System.out.println("1.Show list");
        System.out.println("2.Insert in list");
        System.out.println("3.Delete element on list");
        System.out.println("4.Exit");
		
		
	}
	
	/**
	 * start empty array
	 * @param listaCompra
	 */
	public static void inicializarVacio(String[] listaCompra){
		
		for (int i = 0; i < listaCompra.length; i++) {
			listaCompra[i]=" ";
		}
	}
	
	/**
	 * insert into array
	 * @param listaCompra
	 * @param nuevoElemento
	 * @return
	 */
	public static int insertarElemento (String[] listaCompra, String nuevoElemento){
		int posicion=0;
		for (int i = 0; i < listaCompra.length; i++ ) {
			if (listaCompra[i]== " ") {
				listaCompra[i]=nuevoElemento;
				posicion=i;
			}
		}
		
		return posicion;
	}
	
	/**
	 * delete a one element in array
	 * @param listaCompra
	 * @param cual
	 */
	public static void eliminarElemento (String[] listaCompra, int cual) {
		
		ListaDeLaCompra
	}
	
	
	/**
	 * show array
	 * @param listaCompra
	 */
	public static void mostrarLista (String[] listaCompra) {
		
		for (int i = 0; i < listaCompra.length; i++) {
			System.out.println(i + " " + listaCompra[i]);
		}
		
	}
	
    /**
      * main
      * función principal
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
    	String[] lista= new String[10]; 
        int opcion = 0;
        String linea = "";
        //ListaDeLaCompra list;
        
        ListaDeLaCompra.inicializarVacio(lista);

        BufferedReader consola =new BufferedReader(new InputStreamReader(System.in));
        
        
        salir:
        do {
        	ListaDeLaCompra.mostrarMenu();
        	linea = consola.readLine();
        	opcion = Integer.parseInt(linea);
        
        	switch(opcion) {
        	case 1:
        		ListaDeLaCompra.mostrarLista(lista);
        		break;
        	
        	case 2:
        		System.out.print("Introduzca nuevo elemento: ");
        		linea = consola.readLine();
        		ListaDeLaCompra.insertarElemento(lista,linea);
        		break;
        	
        	case 3: 
        		System.out.print("Introduzca la posicion: ");
        		linea = consola.readLine();
        		opcion=Integer.parseInt(linea);
        		if (opcion <=9 && opcion > 3) {
        			ListaDeLaCompra.eliminarElemento(lista, opcion);
        		}
        		break;
        
        	case 4:
        		break salir;  
        
        	}
        } while(true);
        
        
   
    }

}
