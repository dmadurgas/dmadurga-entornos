package org.cuatrovientos.exercise5class;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Dice myDice = new Dice();
		Dice mySecondDice = new Dice(10,false);
		
		System.out.println(myDice.Launch());
		System.out.println(mySecondDice.Launch());
		
	}

}
