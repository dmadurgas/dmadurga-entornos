package org.cuatrovientos.exercise5class;

import java.util.Random;

public class Dice {

	private int sides=0;
	private boolean isZero=true;
	
	public Dice () {
		this.sides=6;
	}
	
	public Dice(int sides) {
		this.sides=sides;
	}
	public Dice (int sides, boolean isZero) {
		this.sides=sides;
		this.isZero=isZero;
	}
	/**
	 * Launch the master dice
	 * @return
	 */
	public int Launch() {
		Random rnd = new Random();
		if (isZero) {
			return rnd.nextInt(sides);
		}else{
			return (int)(Math.random()*sides+1);
		}
		
	}
}
